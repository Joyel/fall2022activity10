package ca.dawson;

import static org.junit.Assert.*;

import org.junit.Test;

public class UtilitiesTest 
{
    @Test
    public void testConcatenateStringWithString()
    {
        assertEquals("111", Utilities.concatenateStringWithString(3));
    }

    @Test
    public void testConcatenateStringWithStringBuilder(){
        assertEquals("11111", Utilities.concatenateStringWithStringBuilder(5));
    }

    @Test
    public void testNegativeInput(){
        try{
            assertEquals("111", Utilities.concatenateStringWithString(-2));
            fail("Should not be seeing this");
        }

        catch(IllegalArgumentException e){
            System.out.println("Successfully caught");
        }

        try{
            assertEquals("11111", Utilities.concatenateStringWithStringBuilder(-10));
            fail("Should not be seeing this");
        }

        catch(IllegalArgumentException e){
            System.out.println("Successfully caught");
        }

        try{
            assertEquals("111", Utilities.concatenateStringWithString(3));
            assertEquals("11111", Utilities.concatenateStringWithStringBuilder(5));
        }
        catch(IllegalArgumentException e){
            fail("Should not be seeing this");
        }
    }
}
