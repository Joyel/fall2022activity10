package ca.dawson;

public class Utilities {
    public static String concatenateStringWithString(int n){
        if (n < 0){
            throw new IllegalArgumentException("input n can't be negative");
        }
        
        String builder = "";

        for (int i = 0; i < n; i++){
            builder += 1;
        }

        return builder;
    }

    public static String concatenateStringWithStringBuilder(int n){
        if (n < 0){
            throw new IllegalArgumentException("input n can't be negative");
        }
        
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < n; i++){
            builder.append(1);
        }

        return builder.toString();
    }
}
