package ca.dawson;

public class App 
{
    public static void main( String[] args )
    {
        for (int n = 1; n < 1000000000; n *= 2){
            System.out.println("With regular:");
            long time1 = System.nanoTime();
            String stepV1 = Utilities.concatenateStringWithString(n);
            long time2 = System.nanoTime();
            long totalTimeInNano = time2 - time1;
            System.out.println(totalTimeInNano);
            
            System.out.println("With builder:");
            time1 = System.nanoTime();
            String stepV2 = Utilities.concatenateStringWithStringBuilder(n);
            time2 = System.nanoTime();
            totalTimeInNano = time2 - time1;
            System.out.println(totalTimeInNano);
        }
    }
}
